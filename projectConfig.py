import serial

# Configuration of the serial port
serialConfig = {"port":'/dev/ttyUSB0',
                "baudrate": 115200,
                "parity": serial.PARITY_NONE,
                "bytesize": serial.EIGHTBITS,
                "timeout":100}

# Root to the binary file
binaryFileName="/home/safa/Ghassen/embedded/WorkSpace/Application/build/Application.bin"
BOOT_SIZE = 0x4000
APP_START_ADDRESS = 0x8000000 + BOOT_SIZE

FRAME_SIZE = 128
ACK_NACK_FRAME_SIZE = 10

PAGE_SIZE = 1024
