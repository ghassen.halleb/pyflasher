import projectConfig


def read_binary_file(file_name):
    # open("/home/safa/Ghassen/embedded/WorkSpace/testMakefile/build/bbot.bin",'r')
    with open(file_name, 'rb') as file:
        return file.read()


def binary_frame_padding(binary_content):
    frameArray = []
    for i in range(0, (len(binary_content)), 128):
        frameArray.append(binary_content[i:i + 128])
    remaining_data = len(binary_content) % 128
    frameArray.append(binary_content[len(binary_content) - remaining_data: len(binary_content)])
    return frameArray


class PageContent:

    def __init__(self, page_content_array, address):
        self.pageaddress = address
        self.rawPageContent = page_content_array
        # Get the number of frames for a page
        self.frameNumbers = 0
        self.framesContentArray = []
        for i in range(0, len(page_content_array), projectConfig.FRAME_SIZE):
            self.framesContentArray.append(page_content_array[i:i + projectConfig.FRAME_SIZE])
            self.frameNumbers += 1
        remaining_data = len(page_content_array) % projectConfig.FRAME_SIZE
        if remaining_data > 0:
            self.framesContentArray.append(
                page_content_array[len(page_content_array) - remaining_data: len(page_content_array)])
            self.frameNumbers += 1


class BinaryHandler:

    def __init__(self, filename):
        # read data content
        self.file = open(filename, 'rb')
        self.rawContent = self.file.read()
        self.imageSize = len(self.rawContent)
        self.pagesArray = []
        self.checksum = 0

        self.compute_checksum()
        # Get the number of pages
        if len(self.rawContent) % projectConfig.PAGE_SIZE == 0:
            self.pageNumbers = int(len(self.rawContent) / projectConfig.PAGE_SIZE)
        else:
            self.pageNumbers = int(len(self.rawContent) / projectConfig.PAGE_SIZE + 1)

        address = projectConfig.APP_START_ADDRESS
        for i in range(0, len(self.rawContent), projectConfig.PAGE_SIZE):
            self.pagesArray.append(PageContent(self.rawContent[i:i + projectConfig.PAGE_SIZE], address))
            address += projectConfig.PAGE_SIZE
        remaining_data = len(self.rawContent) % projectConfig.PAGE_SIZE
        self.pagesArray.append(
            PageContent(self.rawContent[len(self.rawContent) - remaining_data: len(self.rawContent)], address))

    def compute_checksum(self):
        for i in range(self.imageSize):
            self.checksum = self.checksum ^ self.rawContent[i]
