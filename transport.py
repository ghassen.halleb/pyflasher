import serial
from projectConfig import *
from time import sleep


class Transport:
    RESPONSE_SIZE = 10

    def __init__(self):
        self.ser = serial.Serial(port=serialConfig["port"],
                                 baudrate=serialConfig["baudrate"],
                                 parity=serialConfig["parity"],
                                 bytesize=serialConfig["bytesize"],
                                 timeout=serialConfig["timeout"], )

    def send_frame(self, frame):
        self.ser.write(frame.header.frameHeader)  # write a string
        self.ser.write(frame.data)
        self.ser.write(frame.footer.checksum)

    def receive_response(self):
        return self.ser.read(self.RESPONSE_SIZE)

    def close(self):
        self.ser.close()
