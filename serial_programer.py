from binaryHandler import *
import frameWraper
from transport import *

binaryHandler = BinaryHandler(binaryFileName)
# Binary frames cuts the binary content into 128K fragments
transport_protocol = Transport()
ramWriteCommand = frameWraper.WriteToRamCmd()
flashWriteCommand = frameWraper.WriteToFlashCmd()
jumpToAppCmd = frameWraper.CommandFrame()
checkAppCmd = frameWraper.CheckApplicationCmd(APP_START_ADDRESS, binaryHandler.imageSize, binaryHandler.checksum)
isApplicationValid = False
for page in binaryHandler.pagesArray:

    for frame in page.framesContentArray:
        # Send 128 bytes of data to target RAM
        ramWriteCommand.serialize_frame(frame)
        transport_protocol.send_frame(ramWriteCommand)

        # Get the response from the target
        response = transport_protocol.receive_response()
        if response == "":
            print("Response Error")
            continue
        responseFrame = frameWraper.ResponseFrame()
        responseFrame.deserialize_frame(response)
        if responseFrame.header.command == frameWraper.CommandTypes.ACK_CMD:
            print("Frame number = %d acked" % responseFrame.sourceFrameId)
            response = ""

    # if data sent is equal to a flash page size, perform the writing
    print("Writing page at address %s" % str(hex(page.pageaddress)))
    flashWriteCommand.serialize_frame(page.pageaddress)
    transport_protocol.send_frame(flashWriteCommand)
    response = transport_protocol.receive_response()
    if len(response) != ACK_NACK_FRAME_SIZE:
        print("Response Error")
        continue
    responseFrame = frameWraper.ResponseFrame()
    responseFrame.deserialize_frame(response)
    if responseFrame.header.command == frameWraper.CommandTypes.ACK_CMD:
        print("Frame number = %d acked" % responseFrame.sourceFrameId)
        response = ''

checkAppCmd.serialize_frame()
transport_protocol.send_frame(checkAppCmd)
response = transport_protocol.receive_response()
if len(response) != ACK_NACK_FRAME_SIZE:
    print("Response Error")
responseFrame = frameWraper.ResponseFrame()
responseFrame.deserialize_frame(response)
if responseFrame.header.command == frameWraper.CommandTypes.ACK_CMD:
    print("Frame number = %d acked" % responseFrame.sourceFrameId)
    response = ''
if int(responseFrame.data[0]) == binaryHandler.checksum:
    print("Application is valid!")
    isApplicationValid = True
else:
    print("Application is not valid")

if isApplicationValid:
    print("Jumping to application !")
    dummy_data = [0]
    jumpToAppCmd.serialize_frame(dummy_data, len(dummy_data), frameWraper.CommandTypes.JUMP_TO_APP_CMD)
    transport_protocol.send_frame(jumpToAppCmd)
transport_protocol.close()
