HEADER_SIZE = 3
static_frameId = 0

def compute_checksum(rawDataFrame, length):
    checksum = 0
    if length > len(rawDataFrame):
        return -1
    for i in range(length):
        checksum = checksum ^ rawDataFrame[i]
    return checksum


class CommandTypes:
    NO_CMD = 0x00
    ACK_CMD = 0x01
    W2RAM_CMD = 0x02
    W2FLASH_CMD = 0x03
    FLUSHRAM_CMD = 0x04
    JUMP_TO_APP_CMD = 0x05
    CHECK_APPLICATION_CMD = 0x06
    UNKNOWN_CMD = 0x06

class Header:
    def __init__(self):
        self.length = 0
        self.frameId = static_frameId
        self.command = 0
        self.frameHeader = bytes([])

    def buildHeader(self):
        self.frameHeader = bytes([self.length]) + bytes([self.command]) + bytes([self.frameId])

    def updateFrameId(self):

        global static_frameId
        static_frameId = static_frameId + 1
        # Keep the frame Id 1 byte long
        static_frameId %= 255
        # Increment the frame Id
        self.frameId = static_frameId


class Footer():
    def __init__(self):
        self.checksum = 0


class CommandFrame():

    def __init__(self):
        self.header = Header()
        self.footer = Footer()
        self.data = bytes([])
        self.footer.checksum = bytes([0])
        self.rawData = bytes([])

    def compute_checksum(self):
        checksum = 0
        for i in range(HEADER_SIZE):
            checksum = checksum ^ self.header.frameHeader[i]
        for i in range(self.header.length):
            checksum = checksum ^ self.data[i]
        self.footer.checksum = bytes([checksum])

    def serialize_frame(self, rawDataFrame, length, commandType):

        self.data = rawDataFrame
        self.header.length = length
        self.header.command = commandType

        # build the header of the frame
        self.header.buildHeader()

        # compute the checksum
        self.compute_checksum()

        # update frameId of the next frame
        self.header.updateFrameId()



class ResponseFrame(CommandFrame):

    def __init__(self):
        super().__init__()
        self.sourceFrameId = 0

    def deserialize_frame(self,rawInput):
        self.header.length = rawInput[0]
        self.header.command = rawInput[1]
        self.header.frameId = rawInput[2]
        self.sourceFrameId = rawInput[3]
        self.data = rawInput[4:self.header.length+4]
        self.footer.checksum = rawInput[self.header.length+3]

class WriteToFlashCmd(CommandFrame):
    def __init__(self):
        super().__init__()
        self.command = CommandTypes.W2FLASH_CMD
        self.flash_address = []

    def serialize_frame(self, flash_address):
        self.set_flash_write_address(flash_address)
        super().serialize_frame(self.flash_address, len(self.flash_address), self.command)

    def set_flash_write_address(self, flash_address):
        self.flash_address = flash_address.to_bytes(4, 'little')

class WriteToRamCmd(CommandFrame):
    def __init__(self):
        super().__init__()
        self.command = CommandTypes.W2RAM_CMD
        self.flash_address = []

    def serialize_frame(self, data):
        super().serialize_frame(data,len(data), self.command)

class WriteToFlashCmd(CommandFrame):
    def __init__(self):
        super().__init__()
        self.command = CommandTypes.W2FLASH_CMD
        self.flash_address = []

    def serialize_frame(self, flash_address):
        self.set_flash_write_address(flash_address)
        super().serialize_frame(self.flash_address, len(self.flash_address), self.command)

    def set_flash_write_address(self, flash_address):
        self.flash_address = flash_address.to_bytes(4, 'little')

class CheckApplicationCmd(CommandFrame):
    def __init__(self,start_address, size, checksum):
        super().__init__()
        self.command = CommandTypes.CHECK_APPLICATION_CMD
        self.flash_start_address = start_address.to_bytes(4, 'little')
        self.size = size.to_bytes(4, 'little')
        self.checksum = checksum.to_bytes(1, 'little')

    def serialize_frame(self):
        super().serialize_frame(self.flash_start_address + self.size + self.checksum,
                                len(self.flash_start_address) + len(self.size) + len(self.checksum),
                                self.command)


class WriteToRamCmd(CommandFrame):
    def __init__(self):
        super().__init__()
        self.command = CommandTypes.W2RAM_CMD
        self.flash_address = []

    def serialize_frame(self, data):
        super().serialize_frame(data,len(data), self.command)